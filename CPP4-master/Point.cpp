#include <iostream>
#include "Point.hpp"

Point ORIGINE;

Point::Point(int vx, int vy):x(vx),y(vy)
{
    // std::cout << "Création du point " << this << " de coordonnées :" << std::endl;
    // std::cout << vx << " " << vy << std::endl;
    cpt++;
}

Point::Point():Point(0,0)
{}

const int& Point::getX() const
{
    return x;
}

const int& Point::getY() const
{
    return y;
}

void Point::setX(int v)
{
    x = v;
}

void Point::setY(int v)
{
    y = v;
}

void Point::deplacerDe(int dx, int dy)
{
    x+=dx;
    y+=dy;
}

void Point::deplacerVers(int dx, int dy)
{
    x=dx;
    y=dy;
}

int Point::getCpt()
{
    return cpt;
}

int Point::cpt = 0;