#ifndef __CPP4__FORME_HPP__
#define __CPP4__FORME_HPP__

#include "Point.hpp"

// enum COULEURS { BLANC, NOIR};
enum class COULEURS { BLANC, NOIR, BLEU, ROUGE, VERT, ROSE, VIOLET, JAUNE};

class Forme
{
    Point p;
    COULEURS couleur;
    int w;
    int h;
    int id;
    static int nbFormes;

    public:
        Forme(int, int, int, int);
        Forme(Point, COULEURS);
        Forme();
        Point& getPoint(); // const à gauche : permet de renvoyer un point constant / à droite : permet que la méthode getPoint soit constante.
        // Une référence équivaut à avoir deux variables avec des noms différents sur une même valeur. (EN GROS)
        // Donc quand on renvoie une référence de point, on a le point renvoyé et celui dans le forme qui sont deux points différents en soit, 
        // mais lorsque on modifie un des points, on modifie l'autre.
        void setX(const int); // On peut dire int = const int, mais pas l'inverse <=> a = 5, mais pas 5 = a
        void setY(const int); 

        COULEURS getCouleur() const;
        void setCouleur(COULEURS);

        int getLargeur() const;
        int getHauteur() const;

        int getId() const;
        static int prochainId();

        static int getNbFormes();
        ~Forme();
};

#endif