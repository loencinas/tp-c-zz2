#include <iostream>

#include "Forme.hpp"

Forme::Forme(int vx, int vy, int vw, int vh):p(vx,vy),w(vw),h(vh)
{
    id = nbFormes;
    couleur = COULEURS::BLEU;
    nbFormes++;
}

Forme::Forme(Point np, COULEURS ncouleur):p(np),couleur(ncouleur),w(0),h(0)
{
    id = nbFormes;
    nbFormes++;
}

Forme::Forme():Forme(0,0,0,0)
{}

Point& Forme::getPoint()
{
    return p;
}

void Forme::setX(const int nx)
{
    p.setX(nx);
}

void Forme::setY(const int ny)
{
    p.setY(ny);
}

COULEURS Forme::getCouleur() const
{
    return couleur;
}

void Forme::setCouleur(COULEURS nouvcouleur)
{
    couleur = nouvcouleur;
}

int Forme::getLargeur() const
{
    return w;
}

int Forme::getHauteur() const
{
    return h;
}

int Forme::getId() const
{
    return id;
}

int Forme::prochainId()
{
    return nbFormes;
}

int Forme::getNbFormes()
{
    return nbFormes;
}

int Forme::nbFormes = 0;

Forme::~Forme()
{}