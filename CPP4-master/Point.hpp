#ifndef __CPP4__POINT_HPP__
#define __CPP4__POINT_HPP__

class Point
{
    private:
        int x;
        int y;
        static int cpt;

    public:
        Point(int, int);
        Point();
        const int& getX() const;
        const int& getY() const;
        void setX(int);
        void setY(int);
        void deplacerDe(int, int);
        void deplacerVers(int, int);

        static int getCpt();
};

extern Point ORIGINE;

// declaration d'un point ORIGINE
// il ne faudra pas oublier de l'instancier quelque part
// extern Point ORIGINE;

#endif