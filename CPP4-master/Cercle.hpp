#ifndef __CPP4__CERCLE_HPP__
#define __CPP4__CERCLE_HPP__

#include "Forme.hpp"

class Cercle
{
    private:
        int x;
        int y;
        int w;
        int h;
        int ordre;

    public:
        Cercle(int, int, int, int, int);
        Cercle(int, int, int, int);
        Cercle(int, int, int);
        Cercle();
        std::string toString();
        int getOrdre();
        ~Cercle();
};

#endif
