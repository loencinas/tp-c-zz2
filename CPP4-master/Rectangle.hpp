#ifndef __CPP4__RECTANGLE_HPP__
#define __CPP4__RECTANGLE_HPP__

#include "Forme.hpp"

class Rectangle
{
    private:
        int x;
        int y;
        int w;
        int h;
        int ordre;

    public:
        Rectangle(int, int, int, int, int);
        Rectangle(int, int, int ,int);
        Rectangle();
        std::string toString();
        int getOrdre();
        ~Rectangle();
};

#endif