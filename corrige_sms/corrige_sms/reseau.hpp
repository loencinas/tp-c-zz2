#ifndef __RESEAU__
#define __RESEAU__

#include <list>

class Telephone;

class Reseau {
private:
    std::list<Telephone> _telephones;

public:
    Reseau() = default;

    const std::string lister() const;

    void ajouter(const std::string& numero);
    Telephone& trouveTel(const std::string& numero);
};

#endif