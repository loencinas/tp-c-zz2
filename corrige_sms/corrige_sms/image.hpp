#ifndef __IMAGE__
#define __IMAGE__

#include "media.hpp"

class Image : public Media {
public:
    std::string afficher() const override;
};

#endif
