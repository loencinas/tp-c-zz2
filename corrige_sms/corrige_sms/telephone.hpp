#ifndef __TELEPHONE__
#define __TELEPHONE__

#include <list>
#include <string>

class Message;
class MMS;
class Reseau;

class Telephone {
private:
    std::string _numero;
    Reseau* _reseau;

    std::list<Message*> _messages;

public:
    Telephone();
    Telephone(const std::string& numero, Reseau* reseau = nullptr);
    ~Telephone();

    const std::string& getNumero() const;
    const Reseau* getReseau() const;

    std::size_t getNbMessages() const;

    void ajouter(Message* message);
    void setNumero(const std::string& numero);
    void textoter(const std::string& destinataire, const std::string& texte);
    void mmser(const std::string& destinataire, MMS* mms);

    bool operator<(const Telephone& rhs) const;
    bool operator==(const Telephone& rhs) const;

    // friend bool operator<(const Telephone& lhs, const Telephone& rhs);
};

#endif