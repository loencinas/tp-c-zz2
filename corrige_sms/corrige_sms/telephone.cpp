#include "mauvais_numero.hpp"
#include "message.hpp"
#include "mms.hpp"
#include "reseau.hpp"
#include "sms.hpp"
#include "telephone.hpp"

#include <iostream>

Telephone::Telephone() : Telephone { "", nullptr } {}

Telephone::Telephone(const std::string& numero, Reseau* reseau)
    : _numero { numero }, _reseau { reseau } {}

Telephone::~Telephone() {
    for (auto it { _messages.begin() }; it != _messages.end(); ++it) {
        delete *it;
    }
}

const std::string& Telephone::getNumero() const {
    return _numero;
}

const Reseau* Telephone::getReseau() const {
    return _reseau;
}

std::size_t Telephone::getNbMessages() const {
    return _messages.size();
}

void Telephone::setNumero(const std::string& numero) {
    _numero = numero;
}

void Telephone::ajouter(Message* message) {
    _messages.push_back(message);
    std::cout << "Ajout dans " << _numero << " nb messages = " << getNbMessages() << std::endl;
}

void Telephone::textoter(const std::string& destinataire, const std::string& texte) {
    try {
        auto& tel { _reseau->trouveTel(destinataire) };
        tel.ajouter(new SMS {_numero, destinataire, texte });
    } catch (MauvaisNumero& e) {}

    ajouter(new SMS {_numero, destinataire, texte });
}

void Telephone::mmser(const std::string& destinataire, MMS* mms) {
    try {
        auto& tel { _reseau->trouveTel(destinataire) };
        mms->setDe(_numero);
        mms->setA(destinataire);
        tel.ajouter(mms);
    } catch (MauvaisNumero& e) {}

    ajouter(new MMS { *mms });
}

bool Telephone::operator<(const Telephone& rhs) const {
    return _numero < rhs._numero;
}

bool Telephone::operator==(const Telephone& rhs) const {
    return _numero == rhs._numero;
}
