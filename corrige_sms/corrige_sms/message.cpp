#include "message.hpp"

unsigned int Message::_cle {};

Message::Message(const std::string& de, const std::string& a, const std::string& date)
    : _id { _cle++ }, _de { de }, _a { a }, _date { date } {}

unsigned int Message::getCle() {
    return _cle;
}

const std::string& Message::getDe() const {
    return _de;
}

const std::string& Message::getA() const {
    return _a;
}

void Message::setDe(const std::string& de) {
    _de = de;
}

void Message::setA(const std::string& a) {
    _a = a;
}

unsigned int Message::getId() const {
    return _id;
}