#ifndef __VIDEO__
#define __VIDEO__

#include "media.hpp"

class Video : public Media {
public:
    std::string afficher() const override;
};

#endif
