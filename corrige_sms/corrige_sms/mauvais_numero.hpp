#ifndef __MAUVAIS_NUMERO__
#define __MAUVAIS_NUMERO__

#include <stdexcept>

struct MauvaisNumero : std::invalid_argument {
    MauvaisNumero();
};

#endif