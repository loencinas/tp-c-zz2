#include "media.hpp"
#include "mms.hpp"

MMS::MMS(const MMS& rhs) : MMS { rhs._de, rhs._a, rhs._date } {}

MMS::~MMS() {
    for (auto it { _medias.begin() }; it != _medias.end(); ++it) {
        delete *it;
    }
}

std::string MMS::afficher() const {
    std::string result { SMS::afficher() };

    for (auto it { _medias.begin() }; it != _medias.end(); ++it) {
        result += (*it)->afficher();
    }

    return result;
}

void MMS::joindre(Media* media) {
    _medias.push_back(media);
}
