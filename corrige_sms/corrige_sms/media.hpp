#ifndef __MEDIA__
#define __MEDIA__

#include <string>

class Media {
public:
    virtual ~Media() = default;

    virtual std::string afficher() const = 0;
};

#endif