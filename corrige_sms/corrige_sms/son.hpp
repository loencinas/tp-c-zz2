#ifndef __SON__
#define __SON__

#include "media.hpp"

class Son : public Media {
public:
    std::string afficher() const override;
};

#endif
