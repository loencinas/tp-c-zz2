#ifndef __SMS
#define __SMS

#include <string>

#include "message.hpp"

class SMS : public Message {
private:
    std::string _texte;

public:
    using Message::Message;

    std::string afficher() const override;

    const std::string& getTexte() const;

    void setTexte(const std::string& texte);
};

#endif