#include "sms.hpp"

std::string SMS::afficher() const {
    return getTexte();
}

const std::string& SMS::getTexte() const {
    return _texte;
}

void SMS::setTexte(const std::string& texte) {
    _texte = texte;
}
