#ifndef __MMS__
#define __MMS__

#include <list>

#include "sms.hpp"

class Media;

class MMS : public SMS {
private:
    std::list<Media*> _medias;

public:
    using SMS::SMS;
    MMS(const MMS& rhs);
    ~MMS() override;

    std::string afficher() const override;

    void joindre(Media* media);
};

#endif