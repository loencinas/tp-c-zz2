#ifndef __MESSAGE__
#define __MESSAGE__

#include <string>

class Message {
protected:
    static unsigned int _cle;

    const unsigned int _id;

    std::string _de;
    std::string _a;
    std::string _date;

public:
    Message(const std::string& de, const std::string& a, const std::string& date);
    virtual ~Message() = default;

    virtual std::string afficher() const = 0;

    const std::string& getDe() const;
    const std::string& getA() const;

    void setDe(const std::string& de);
    void setA(const std::string& a);

    static unsigned int getCle();

    unsigned int getId() const;
};

#endif