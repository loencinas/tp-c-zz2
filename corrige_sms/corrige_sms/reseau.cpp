#include <algorithm>

#include "mauvais_numero.hpp"
#include "reseau.hpp"
#include "telephone.hpp"

const std::string Reseau::lister() const {
    std::string result;

    for (auto it { _telephones.begin() }; it != _telephones.end(); ++it) {
        result += it->getNumero() + "\n";
    }

    return result;
}

void Reseau::ajouter(const std::string& numero) {
    _telephones.push_back({ numero, this });
    _telephones.sort();
}

Telephone& Reseau::trouveTel(const std::string& numero) {
    auto it { std::find(_telephones.begin(), _telephones.end(), numero) };

    if (it == _telephones.end()) {
        throw MauvaisNumero();
    }

    return *it;
}
