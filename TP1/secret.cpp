#include <iostream>

int main(int, char **)
{
    using std::string; using std::endl;
    using std::cout; using std::cin;

    string s1, s2("chaine 2"), s3 = "chaine 3";

    cout << s2 << endl; // affichage sur la sortie standard
    cout << s3 << endl; // affichage sur la sortie standard
    cin  >> s3;

    cout << (s1.empty()?"chaine vide":"chaine non vide") << endl;
    
    // taille des chaînes
    cout << s1.length() << endl;
    cout << s2.size()   << endl;

    // vider la chaîne
    s3.clear();

    getline(cin, s3);
    cout << s3 + "" << s3.length() << endl;
}