#include <iostream>
#include <string>

int main(int, char **)
{
  using std::cout; using std::cin;
  using std::endl; using std::string;

  string chaine1;  
  string chaine2;

  cout << "1ère chaîne :" << endl;
  cin  >> chaine1;
  cout << "2ème chaîne :" << endl;
  cin  >> chaine2;

  if(chaine1>chaine2)
  {
    cout << "La chaine la plus grande est : " << chaine1 << endl;
    cout << "Sa longueur est : " << chaine1.length() << endl;
  }
  else
  {
    cout << "La chaine la plus grande est : " << chaine2 << endl;
    cout << "Sacode  longueur est : " << chaine2.length() << endl;
  }

  return 0;
}