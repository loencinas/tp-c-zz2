#include <iostream>

void fonction1(int a)
{
  std::cout << &a << std::endl;
}

void fonction2(int &a)
{
  std::cout << &a << std::endl;
}

void swapp(int *a, int *b)
{
    int c = *a;
    *a = *b;
    *b = c;
}

void swapr(int &a, int &b)
{
    int c = a;
    a = b;
    b = c;
}

int main(int, char **)
{
    using std::cout; using std::cin;
    using std::endl;

/*----------------------------------------------------*/

    /*
    int  a = 3;
    int  b = a;
    int &c = a;

    std::cout << a << " " << b << " " << c << std::endl;
    b = 4;
    std::cout << a << " " << b << " " << c << std::endl;
    c = 5;
    std::cout << a << " " << b << " " << c << std::endl;
    a = 6;
    std::cout << a << " " << b << " " << c << std::endl;

    int x = 5, y = 10;

    std::cout << x << " " << y << std::endl;

    swapp(&x,&y);

    std::cout << x << " " << y << std::endl;

    swapr(x,y);

    std::cout << x << " " << y << std::endl;

    return 0;
    */

/*----------------------------------------------------*/

    /*
    int   a = 4;
    int * p = nullptr;

    p = &a;
    std::cout << *p << " " << p << std::endl;

    return 0;
    */

/*----------------------------------------------------*/

    /*
    int * p = new int;

    *p = 3;
    cout << *p << endl;

    delete p;

    return 0;
    */

/*----------------------------------------------------*/

   const int TAILLE = 500;

   int * p = new int[TAILLE];

   for(auto i = 0; i< TAILLE; ++i ) p[i] = i;
   for(auto i = 0; i< TAILLE; ++i ) cout << p[i] << endl;

   delete [] p;      

   return 0;

}
