#include "Pile.hpp"

Pile::Pile(const int& t):taille(t),indice_sommet(0),tab(new int[t])
{
    std::cout << "Construction d'une pile de taille" << t << std::endl;
}

Pile::Pile():Pile(10)
{}

Pile::~Pile()
{
    std::cout << "Destruction d'une pile" << std::endl;
    delete[] tab;
}

Pile::Pile(const Pile&)
{
    std::cout << "Création par copie d'une pile" << std::endl;
}

Pile& Pile::operator=(const Pile&)
{
    std::cout << "Affection d'une pile" << std::endl;
    return *this;
}

bool Pile::empty() const
{
    return (indice_sommet == 0);
}

const int& Pile::size() const
{
    return indice_sommet;
}

// le const à droite permet de dire que la méthode ne peut pas modifier les attributs.
// or quand on renvoie une référence, on peut modifier l'attribut puisque en modifiant la ref, on modifie l'attribut
// donc, si la méthode est const, soit on renvoie une copie, soit on renvoie un const ref.

const int& Pile::top() const
{
    return tab[indice_sommet-1];
}

void Pile::push(const int& nouv_element)
{
    tab[indice_sommet] = nouv_element;
    indice_sommet++;
}

void Pile::pop()
{
    // On laisse le code le plus simple, la vérification que la pile n'est pas vide se fera par l'utilisateur.
    indice_sommet--;
}