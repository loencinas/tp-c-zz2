#ifndef PILE_HPP
#define PILE_HPP

#include <iostream>

class Pile
{
    int taille;
    int indice_sommet;
    int * tab;

    public:
        Pile(const int&);
        Pile();
        ~Pile();
        Pile(const Pile&);
        Pile& operator=(const Pile&);

        bool empty() const;
        const int& size() const;
        const int& top() const;
        void push(const int&);
        void pop();
};

#endif
