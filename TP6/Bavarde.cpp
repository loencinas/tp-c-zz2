#include <string>
#include <iostream>

class Bavarde
{
    std::string nom;

    public :
        Bavarde(std::string n):nom(n)
        {
            std::cout << "constructeur " << nom << std::endl;
        }
        ~Bavarde()
        {
            std::cout << "destructeur " << nom << std::endl;
        }
};

Bavarde g("global");

int main(int, char **)
{
    Bavarde t("local");
    static Bavarde s("statlocal");

    // default : tous les destructeurs sont appelés.
    //std::exit(1); // le destructeur de local n'est pas apppelé.
    //std::terminate(); // core dumped
    //std::unexpected(); // ne s'appelle pas normalement // core dumped
    return 0;
}