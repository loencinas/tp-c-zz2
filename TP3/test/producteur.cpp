#include "producteur.hpp"
#include <fstream>
#include <cmath>

Producteur::Producteur():travail(0)
{}

int Producteur::getTravail()
{
    return travail;
}

bool Producteur::produire(int quantite, std::string nom)
{
    std::ofstream fichier(nom.c_str());
        
        if(fichier)
        {
            fichier << std::to_string(quantite) + "\n";
            for(int i = 1;i<quantite+1;i++)
            {
                fichier << std::to_string(i);
                if(i != quantite) fichier << "\n";
            }
            fichier.close();
        }
        else
        {
            std::cerr << "Erreur à l'ouverture !" << std::endl;
        }

    travail++;
    return 1;
}

Producteur::~Producteur()
{}