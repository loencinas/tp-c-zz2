#include <iostream>
#include <cstdlib>

#include "Bavarde.hpp"

// Bavarde globale(2);

int main(int, char**)
{
    /* // 1.1

    Bavarde b1(2);
    Bavarde b2;
    Bavarde * b3 = new Bavarde(3);
    Bavarde * b4 = new Bavarde[1];

    fonction(globale);

    delete b3;
    delete[] b4;

    */

    /* // 1.2

    const int TAILLE = 20;
    Bavarde   tab1[TAILLE];
    Bavarde * tab2 = new Bavarde[TAILLE];

    for(int i=0; i<TAILLE; ++i)
    {
        tab1[i].afficher();
        tab2[i].afficher();
    }

    delete[] tab2;

    */

    /* // 1.3

    Couple c1(1,2);
    Couple c2(1,1);
    Couple c3(2,2);

    */

    /* // 1.4

    Famille f(3);

    */

    /* // 1.5

    Bavarde * b = (Bavarde*)malloc(sizeof(Bavarde));
    free(b);

    */

    // 2

    //Mere m(1);
    //std::cout << Mere::getCpt() << " " << m.getCpt() << std::endl;
    //Fille f(2);
    //std::cout << Mere::getCpt() << " " << f.getCpt() << std::endl;

    /*

    Mere n(std::string("daronne"));
    Fille e(std::string("gamine"));
    std::cout << n.getName() << "->" << e.getName() << std::endl;
    n.afficher();
    e.afficher();

    */

    Mere  *pm = new Mere("mere_dyn");
    Fille *pf = new Fille("fille_dyn");
    Mere  *pp = new Fille("fille vue comme mere");
    pm->afficher(); // affiche Mere
    pf->afficher(); // affiche Fille
    pp->afficher(); // affiche Fille
    delete pm;
    delete pf;
    delete pp;

    return 0;

    // bizarre ?
}