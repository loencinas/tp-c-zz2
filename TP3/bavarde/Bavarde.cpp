#include <iostream>
#include "Bavarde.hpp"

Bavarde::Bavarde(int v):val(v)
{
    std::cout << "Construction de " << v << std::endl;
}

Bavarde::Bavarde():Bavarde(0)
{}

Bavarde::~Bavarde()
{
    std::cout << "Tais-toi " << val << std::endl;
}

int Bavarde::get()
{
    return val;
}

void Bavarde::afficher()
{
    std::cout << "Affichage de " << val << std::endl;
}

Couple::Couple(int i1, int i2):bav1(Bavarde(i1)),bav2(Bavarde(i2))
{
    std::cout << "Création !" << std::endl;
}

Couple::~Couple()
{
    std::cout << "Destruction !" << std::endl;
}

Famille::Famille(int taille):tb(new Bavarde[taille])
{
    // Il suffit de juste mettre la ligne suivante pour que une taille de tableau
    // de 0 soit admissible
    // tb = new Bavarde[taille]; // marche aussi
}

Famille::~Famille()
{
    delete[] tb;
}

Mere::Mere(std::string s):name(s)
{
    std::cout << s << std::endl;
    ++cpt;
}

Mere::Mere(int v):val(v)
{
    std::cout << "Mere " << v << std::endl;
    ++cpt;
}

Mere::Mere():Mere(0)
{}

Mere::~Mere()
{
    std::cout << "Mere" << " détruite" << std::endl;
}

std::string Mere::getName()
{
    return name;
}

void Mere::afficher()
{
    std::cout << "Je suis de classe Daronne" << std::endl;
}

int Mere::getCpt()
{
    return cpt;
}

int Mere::cpt = 0;

Fille::Fille(std::string s):Mere(s)
{}

Fille::Fille(int v):Mere(v)
{
    std::cout << "Fille " << v << std::endl;
}

Fille::Fille():Fille(0)
{}

Fille::~Fille()
{
    std::cout << "Fille" << " détruite" << std::endl;
}

void Fille::afficher()
{
    std::cout << "Je suis de classe Fille" << std::endl;
}

void fonction(Bavarde b)
{
    std::cout << "J'affiche " << b.get() << std::endl;
}