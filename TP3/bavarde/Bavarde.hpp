#ifndef _BAVARDE_HPP_
#define _BAVARDE_HPP_

class Bavarde
{
    int val;

    public:
        Bavarde(int);
        Bavarde();
        ~Bavarde();
        int get();
        void afficher();
};

class Couple
{
    Bavarde bav1;
    Bavarde bav2;

    public:
        Couple(int,int);
        ~Couple();
};

class Famille
{
    Bavarde * tb;

    public:
        Famille(int);
        ~Famille();
};

class Mere
{
    static int cpt;
    std::string name;

    protected:
        int val; // utilisé un getter si privé pour l'avoir dans Fille

    public:
        Mere(std::string);
        Mere(int);
        Mere();
        ~Mere();
        std::string getName();
        void afficher();
        static int getCpt();
};

class Fille : public Mere
{
    public:
        Fille(std::string);
        Fille(int);
        Fille();
        ~Fille();
        void afficher();
};

void fonction(Bavarde b);

#endif