#ifndef _RECTANGLE_HPP_
#define _RECTANGLE_HPP_

class Rectangle
{
    private:
        int x;
        int y;
        int w;
        int h;
        static int cpt;

    public:
        Rectangle(int, int, int ,int);
        Rectangle();
        std::string toString();
        static int getCpt();

};

#endif