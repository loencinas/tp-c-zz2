#include <iostream>
#include <string>
#include "Point.hpp"
#include "Rectangle.hpp"
#include "Cercle.hpp"

int main(int, char**)
{
    /*

    std::cout << Point::getCpt() << std::endl;

    Point p(2,1);
    Point p1;

    Point * tp = new Point[10];

    std::cout << p.getCpt() << std::endl;

    delete [] tp;

    */

    std::cout << Rectangle::getCpt() << std::endl;
    Rectangle * rec = new Rectangle();
    std::cout << Rectangle::getCpt() << std::endl;
    delete rec;

    Rectangle r;
    std::cout << r.toString() << std::endl;

    Cercle c(0,0,1);
    std::cout << c.toString() << std::endl;
    

    return 0;
}