#include <iostream>
#include <string>
#include "Rectangle.hpp"

Rectangle::Rectangle(int vx, int vy, int vw, int vh):x(vx),y(vy),w(vw),h(vh)
{
    cpt++;
}

Rectangle::Rectangle():Rectangle(0,0,0,0)
{}

std::string Rectangle::toString()
{
    std::string str = "RECTANGLE " + std::to_string(this->x) + " " + std::to_string(this->y) + " " + std::to_string(this->w) + " " + std::to_string(this->h);
    return str;
}

int Rectangle::getCpt()
{
    return cpt;
}

int Rectangle::cpt=0;