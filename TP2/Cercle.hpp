#ifndef _CERCLE_HPP_
#define _CERCLE_HPP_

class Cercle
{
    private:
        int x;
        int y;
        int w;
        int h;

    public:
        Cercle(int, int, int, int);
        Cercle(int, int, int);
        Cercle();
        std::string toString();
};

#endif