#include <iostream>

#include "Pile.hpp"

using namespace std;

template<typename T>
const T& maximum(const T& a,const T& b)
{
    return ((a > b) ? a : b);
}

int main(int,char**)
{
    int a = 15, b = 1;
    double c = 10., d = -1.;
    cout << maximum(a,b) << endl;
    cout << maximum(c,d) << endl;
    cout << maximum(a,(int)c) << endl;
    return 0;
}