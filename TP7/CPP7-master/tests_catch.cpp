#include "catch.hpp"
#include "Pile.hpp"

TEST_CASE("Constructeur par defaut")
{
   typedef Pile<int> Pile_entier;
   Pile_entier p; // cela implique que par defaut la capacite de la pile n'est pas nulle => pas d exception

   CHECK(  p.empty() );
   CHECK(  0 == p.size() );

   p.push(5);

   CHECK( !p.empty() );
   CHECK( 1 == p.size() );
   CHECK( 5 == p.top() );

   p.push(2);
   p.push(1);

   CHECK( 3 == p.size() );
   CHECK( 1 == p.top() );

   p.pop();

   CHECK( 2 == p.size() );
   CHECK( 2 == p.top() );

   p.pop();
   p.pop();

   CHECK( 0 == p.size() );
}