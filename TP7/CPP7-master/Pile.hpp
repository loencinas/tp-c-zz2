#ifndef PILE_HPP
#define PILE_HPP

#include <iostream>

template<typename T>
class Pile
{
    int taille;
    int indice_sommet;
    T * tab;

    public:
        Pile(const int&);
        Pile();
        ~Pile();
        Pile(const Pile&);
        Pile& operator=(const Pile&);

        bool empty() const;
        const int& size() const;
        const T& top() const;
        void push(const T&);
        void pop();
};

template<typename T>
Pile<T>::Pile(const int& t):taille(t),indice_sommet(0),tab(new T[t])
{
    std::cout << "Construction d'une pile de taille" << t << std::endl;
}

template<typename T>
Pile<T>::Pile():Pile(10)
{}

template<typename T>
Pile<T>::~Pile()
{
    std::cout << "Destruction d'une pile" << std::endl;
    delete[] tab;
}

template<typename T>
Pile<T>::Pile(const Pile&)
{
    std::cout << "Création par copie d'une pile" << std::endl;
}

template<typename T>
Pile<T>& Pile<T>::operator=(const Pile&)
{
    std::cout << "Affection d'une pile" << std::endl;
    return *this;
}

template<typename T>
bool Pile<T>::empty() const
{
    return (indice_sommet == 0);
}

template<typename T>
const int& Pile<T>::size() const
{
    return indice_sommet;
}

// le const à droite permet de dire que la méthode ne peut pas modifier les attributs.
// or quand on renvoie une référence, on peut modifier l'attribut puisque en modifiant la ref, on modifie l'attribut
// donc, si la méthode est const, soit on renvoie une copie, soit on renvoie un const ref.

template<typename T>
const T& Pile<T>::top() const
{
    return tab[indice_sommet-1];
}

template<typename T>
void Pile<T>::push(const T& nouv_element)
{
    tab[indice_sommet] = nouv_element;
    indice_sommet++;
}

template<typename T>
void Pile<T>::pop()
{
    // On laisse le code le plus simple, la vérification que la pile n'est pas vide se fera par l'utilisateur.
    indice_sommet--;
}

#endif
