#include "capacite.hpp"

Capacite::Capacite(const string& nom, const int& niveau):_nom(nom),_niveau(niveau)
{}

const int& Capacite::getNiveau()
{
    return _niveau;
}

const string& Capacite::getNom()
{
    return _nom;
}