#include "physique.hpp"

ostream& Physique::exercer(ostream& os)
{
    os << _nom << " [" << _niveau << "]";
    return os;
}

ostream& Physique::utiliser(ostream& os)
{
    return exercer(os);
}