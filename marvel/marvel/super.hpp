#ifndef __SUPER__
#define __SUPER__

#include <list>

#include "marvel.hpp"

#include "personne.hpp"
#include "capacite.hpp"
#include "anonymeexception.hpp"

class Super
{
    private:
        string _nom;
        Personne _identite;
        bool _anonyme = true;
        std::list<Capacite*> _capacites;

    public:
        Super(string,Personne);
        Super();
        ~Super();

        string getNom() const;
        bool estAnonyme() const;
        void enregistrer();
        const Personne& getIdentite() const;
        void setIdentite(const Personne&);
        void ajouter(Capacite*);
        int getNiveau();
        const list<Capacite*> getCapacites();
};

#endif