#ifndef __CAPACITE__
#define __CAPACITE__

#include "marvel.hpp"

class Capacite
{
    protected:
        string _nom;
        int _niveau;

    public:
        Capacite(const string&, const int&);
        virtual ~Capacite() = default;
        virtual ostream& utiliser(ostream&) = 0;
        const string& getNom();
        const int& getNiveau();
};

#endif