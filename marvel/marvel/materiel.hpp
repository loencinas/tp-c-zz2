#ifndef __MATERIEL__
#define __MATERIEL__

#include "marvel.hpp"

#include "capacite.hpp"

class Materiel : public Capacite
{
    public:
        using Capacite::Capacite;
        ostream& actionner(ostream&);
        ostream& utiliser(ostream&) override;
};

#endif