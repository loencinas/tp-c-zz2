#include "super.hpp"

Super::Super(string nom, Personne identite):_nom(nom),_identite(identite)
{}

Super::Super():Super("Deadpool",Personne("Wade","Wilson",Personne::HOMME))
{}

Super::~Super()
{
    for (auto it { _capacites.begin() }; it != _capacites.end(); ++it) {
        delete *it;
    }
}

string Super::getNom() const
{
    return _nom;
}

bool Super::estAnonyme() const
{
    return _anonyme;
}

void Super::enregistrer()
{
    _anonyme = !_anonyme;
}

const Personne& Super::getIdentite() const
{
    if(_anonyme)
    {
        throw AnonymeException();
    }
    return _identite;
}

void Super::setIdentite(const Personne& p)
{
    _identite = p;
    _anonyme = true;
}


void Super::ajouter(Capacite* capa)
{
    _capacites.push_back(capa);
    cout << "Ajout de la capacite " << capa->getNom() << " a " << _nom << endl;
}


int Super::getNiveau()
{
    int nivGlobal = 0;

    for (Capacite* capa : _capacites)
    {
        nivGlobal += capa->getNiveau() ;
    }

    return nivGlobal;
}

const list<Capacite*> Super::getCapacites()
{
    return _capacites;
}