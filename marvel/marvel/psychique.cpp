#include "psychique.hpp"

ostream& Psychique::penser(ostream& os)
{
    os << _nom << " [" << _niveau << "]";
    return os;
}

ostream& Psychique::utiliser(ostream& os)
{
    return penser(os);
}