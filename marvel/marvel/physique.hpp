#ifndef __PHYSIQUE__
#define __PHYSIQUE__

#include "marvel.hpp"

#include "capacite.hpp"

class Physique : public Capacite
{
    public:
        using Capacite::Capacite;
        ostream& exercer(ostream&);
        ostream& utiliser(ostream&) override;
};

#endif