#include "materiel.hpp"

ostream& Materiel::actionner(ostream& os)
{
    os << _nom << " [" << _niveau << "] en action";
    return os;
}

ostream& Materiel::utiliser(ostream& os)
{
    return actionner(os);
}