#include "personne.hpp"

Personne::Personne(string prenom, string nom, Genre genre):_prenom(prenom),_nom(nom),_genre(genre)
{}

Personne::Personne(string prenom, string nom):Personne(prenom,nom,Personne::INDETERMINE)
{}

Personne::Personne():Personne("Gerard","Depardieu",Personne::HOMME)
{}

Personne::~Personne()
{}

bool operator==(const Personne& p1, const Personne& p2)
{
    return ((p1.getPrenom() == p2.getPrenom()) && (p1.getNom() == p2.getNom()) && (p1.getGenre() == p2.getGenre()));
}

ostream& operator<<(ostream& os,const Personne& p)
{
    return p.afficher(os);
}

const string& Personne::getPrenom() const
{
    return _prenom;
}

const string& Personne::getNom() const 
{
    return _nom;
}

const Personne::Genre& Personne::getGenre() const
{
    return _genre;
}

ostream& Personne::afficher(ostream& os) const
{
    string ngenre;

    switch(_genre)
    {
        case 0:
            ngenre = "HOMME";
            break;
        case 1:
            ngenre = "FEMME";
            break;
        case 2:
            ngenre = "INDETERMINE";
            break;
    }
    
    os << _prenom << " " << _nom << " [" << ngenre << "]";
    return os;
}

Personne Personne::INCONNU("Inconnu","Inconnu",Personne::INDETERMINE);