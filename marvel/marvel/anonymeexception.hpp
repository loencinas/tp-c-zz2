#ifndef __ANONYMEEXCEPTION__
#define __ANONYMEEXCEPTION__

#include "marvel.hpp"

class AnonymeException : public exception
{
    public:
        const char* what();
};

#endif