#ifndef __PERSONNE__
#define __PERSONNE__

#include "marvel.hpp"

class Personne
{
    public:
        enum Genre {HOMME,FEMME,INDETERMINE};
        static Personne INCONNU;

    private :
        string _prenom;
        string _nom;
        Genre _genre;

    public:
        Personne(string, string, Genre);
        Personne(string, string);
        Personne();
        ~Personne();
        friend bool operator==(const Personne&,const Personne&);
        friend ostream& operator<<(ostream&,const Personne&);

        const string& getPrenom() const;
        const string& getNom() const;
        const Genre& getGenre() const;
        ostream& afficher(ostream& os) const;
};

#endif