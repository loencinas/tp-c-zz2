#ifndef __PSYCHIQUE__
#define __PSYCHIQUE__

#include "marvel.hpp"

#include "capacite.hpp"

class Psychique : public Capacite
{
    public:
        using Capacite::Capacite;
        ostream& penser(ostream&);
        ostream& utiliser(ostream&) override;
};

#endif