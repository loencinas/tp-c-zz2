#include <iostream>

#include "Forme.hpp"

Forme::Forme(int vx, int vy, int vw, int vh):
x(vx),y(vy),w(vw),h(vh)
{
    nbFormes++;
}

Forme::Forme():Forme(0,0,0,0)
{}

int Forme::getNbFormes()
{
    return nbFormes;
}

int Forme::nbFormes = 0;

Forme::~Forme()
{}