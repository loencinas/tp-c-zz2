#ifndef LISTE_HPP
#define LISTE_HPP

#include "Cercle.hpp"
#include "Rectangle.hpp"

class Liste
{
    public :
        Cercle * tabCercle;
        Rectangle * tabRectangle;

    private: 
        static int const taille = 10;
        int nb_c;
        int nb_r;
        int longueur;
        static int compteur;

    public:
        Liste();
        static int getCompteur();
        void ajoutCercle(int, int, int, int);
        void ajoutRectangle(int, int, int, int);
        std::string toString();
        int getLongueur();
        ~Liste();
};

#endif