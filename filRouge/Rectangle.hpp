#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

class Rectangle
{
    private:
        int x;
        int y;
        int w;
        int h;
        int ordre;

    public:
        Rectangle(int, int, int, int, int);
        Rectangle(int, int, int ,int);
        Rectangle();
        std::string toString();
        int getOrdre();
        ~Rectangle();
};

#endif