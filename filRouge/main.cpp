#include <iostream>
#include <string>

#include "Point.hpp"
#include "Cercle.hpp"
#include "Rectangle.hpp"
#include "Liste.hpp"
#include "Forme.hpp"

int main(int, char**)
{
    Liste l;
    l.ajoutCercle(10,10,2,2);
    l.ajoutRectangle(10,1,2,3);
    l.ajoutRectangle(100,0,0,0);
    l.ajoutCercle(4,5,2,3);
    l.ajoutRectangle(9,9,9,9);

    std::cout << l.toString();
    std::cout << "Longueur de la liste : " << l.getLongueur() << std::endl;

    return 0;
}