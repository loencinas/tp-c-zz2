#ifndef _POINT_HPP_
#define _POINT_HPP_

class Point
{
    private:
        int x;
        int y;
        static int cpt;

    public:
        Point(int, int);
        Point();
        int getX();
        int getY();
        void setX(int);
        void setY(int);
        void deplacerDe(int, int);
        void deplacerVers(int, int);

        static int getCpt();
};

#endif