#ifndef CERCLE_HPP
#define CERCLE_HPP

class Cercle
{
    private:
        int x;
        int y;
        int w;
        int h;
        int ordre;

    public:
        Cercle(int, int, int, int, int);
        Cercle(int, int, int, int);
        Cercle(int, int, int);
        Cercle();
        std::string toString();
        int getOrdre();
        ~Cercle();
};

#endif