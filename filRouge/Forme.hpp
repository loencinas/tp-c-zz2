#ifndef FORME_HPP
#define FORME_HPP

class Forme
{
    int x;
    int y;
    int w;
    int h;
    static int nbFormes;

    public:
        Forme(int, int, int, int);
        Forme();
        static int getNbFormes();
        ~Forme();
};

#endif