#include <iostream>
#include <string>

#include "Cercle.hpp"

Cercle::Cercle(int vx, int vy, int vw, int vh, int vordre):
x(vx),y(vy),w(vw),h(vh),ordre(vordre)
{}

Cercle::Cercle(int vx, int vy, int vw, int vh):Cercle(vx,vy,vw,vh,0)
{}

Cercle::Cercle(int vx, int vy, int rayon):Cercle(vx,vy,2*rayon,2*rayon,0)
{}

Cercle::Cercle():Cercle(0,0,0,0,0)
{}

std::string Cercle::toString()
{
    std::string str = "CERCLE " + std::to_string(this->x) + " " + std::to_string(this->y) + " " + std::to_string(this->w) + " " + std::to_string(this->h);
    return str;
}

int Cercle::getOrdre()
{
    return ordre;
}

Cercle::~Cercle()
{}