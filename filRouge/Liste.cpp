#include <iostream>
#include <string>

#include "Liste.hpp"

Liste::Liste():
tabCercle(new Cercle[taille]),tabRectangle(new Rectangle[taille]),
nb_c(0),nb_r(0),longueur(0)
{
    ++compteur;
}

int Liste::getCompteur()
{
    return compteur;
}

int Liste::compteur = 0;

void Liste::ajoutCercle(int vx, int vy, int vw, int vh)
{
    tabCercle[nb_c] = Cercle(vx,vy,vw,vh,nb_c+nb_r);
    nb_c++;
    longueur++;
}

void Liste::ajoutRectangle(int vx, int vy, int vw, int vh)
{
    tabRectangle[nb_r] = Rectangle(vx,vy,vw,vh,nb_c+nb_r);
    nb_r++;
    longueur++;
}

std::string Liste::toString()
{
    std::string str;
    int cer = 0; int rec = 0;
    for(int i = 0;i<longueur;i++)
    {
        if(cer < nb_c && rec < nb_r)
        {
            if(tabCercle[cer].getOrdre() < tabRectangle[rec].getOrdre())
            {
                str += tabCercle[cer].toString() + " ordre : " + std::to_string(tabCercle[cer].getOrdre());
                str += "\n";
                cer++;
            }
            else
            {
                str += tabRectangle[rec].toString() + " ordre : " + std::to_string(tabRectangle[rec].getOrdre());
                str +="\n";
                rec++;
            }
        }
        else
        {
            if(cer == nb_c && rec < nb_r)
            {
                str += tabRectangle[rec].toString() + " ordre : " + std::to_string(tabRectangle[rec].getOrdre());
                str +="\n";
                rec++; 
            }
            if(rec == nb_r && cer < nb_c)
            {
                str += tabCercle[cer].toString() + " ordre : " + std::to_string(tabCercle[cer].getOrdre());
                str += "\n";
                cer++;
            }
        }
    }
    return str;
}

int Liste::getLongueur()
{
    return longueur;
}

Liste::~Liste()
{
    delete[] tabCercle;
    delete[] tabRectangle;
}
