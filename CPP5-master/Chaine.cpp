#include "Chaine.hpp"
#include <cstring>

Chaine::Chaine(const char * inCS):capacite(strlen(inCS)),tab(new char[capacite])
{
    strcpy(tab,inCS);
}

Chaine::Chaine(const int inCapacite):capacite(inCapacite),tab(nullptr)
{}

Chaine::Chaine():capacite(-1),tab(nullptr)
{}

int Chaine::getCapacite() const
{
    return capacite;
}

char * Chaine::c_str() const
{
    return tab;
}

Chaine::~Chaine()
{
    delete tab;
}