#ifndef CPP5__CHAINE_HPP
#define CPP5__CHAINE_HPP

#include <iostream>
#include <fstream>

class Chaine
{
    int capacite;
    char * tab;

    public:
        Chaine(const char *);
        Chaine(const int);
        Chaine();
        int getCapacite() const;
        char * c_str() const;
        ~Chaine();
};

#endif
